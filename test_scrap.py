# http://selenium-tutorial.blogspot.co.id/2014/06/staleelementreferenceexception-webdriver.html
# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import WebDriverException,ElementNotVisibleException,NoSuchElementException,StaleElementReferenceException
import unittest, time, re

# import pandas as pd
import csv
import pandas as pd
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

path = 'test/booktest.csv'
result_path = 'test/hasil.csv'
error_path = 'error/error.csv'

chrome_request_header = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36"
firefox_request_header = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:43.0) Gecko/20100101 Firefox/43.0"

class PAC(unittest.TestCase):
    def setUp(self):
        self.begin = 142
        self.end = None
        
        dcap = dict(DesiredCapabilities.PHANTOMJS)
        dcap["phantomjs.page.settings.userAgent"] = (
            chrome_request_header
        )

        # self.driver = webdriver.Firefox()
        self.driver = webdriver.PhantomJS(desired_capabilities=dcap)
        self.driver.set_window_size(800, 600)
        
        # self.df = pd.read_csv(file)
        self.reader = pd.read_csv(path, header=None)
        self.writer = csv.writer(open(result_path, 'a', 0))
        self.writerErr = csv.writer(open(error_path, 'a', 0))
        
        self.driver.implicitly_wait(1)
        self.base_url = "https://www.google.co.id"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test(self):
        driver = self.driver
        writer = self.writer
        reader = self.reader
        driver.get(self.base_url + "/maps")
        for i in range(30):
            try:
                if self.is_element_present(By.XPATH, "//div[@id='gb']/div/div"): break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        for index, row in reader.loc[self.begin:self.end].iterrows():
            not_found = False
            self.index = index
            # place = "KOMP. PASAR GANEFO JAKARTA BARAT"
            place = row[0]
            anomaly = ''
            result = ''
            location_icon = 'div.suggest-icon-container.maps-sprite-suggest-place-pin'
            search_icon = 'div.suggest-icon-container.maps-sprite-suggest-search'
            
            print '['+str(index)+']'
            print 'Data: ' + place
            
            try:
                # Suggestion method
                driver.find_element_by_id("searchboxinput").clear()
                driver.find_element_by_id("searchboxinput").send_keys(place)
                for i in range(30):
                    try:
                        if self.is_element_present(By.CSS_SELECTOR, "div.suggest"): break
                    except: pass
                    time.sleep(1)
                else:
                    anomaly = 'suggestion not appear when type'
                    self.verificationErrors.append([str(index),'div.suggest not found'])
                
                # Checking
                try:  
                    self.assertNotEqual("Tambahkan tempat yang belum ada ke Google Maps.", driver.find_element_by_css_selector("div.suggest").text)
                    self.assertTrue(self.is_element_present(By.CSS_SELECTOR, location_icon))
                    suggest_name = driver.find_element_by_css_selector("div.suggest").text
                    driver.find_element_by_css_selector("div.suggest").click()
                except AssertionError as e: 
                        # fail? then use Quick Look method
                    driver.find_element_by_css_selector("button.searchbox-searchbutton").click()
                        
                        # Accessing LIST of all Similiar Places if its still available
                    for i in range(1):
                        try:
                            if self.is_element_present(By.CSS_SELECTOR, "div.widget-pane-section-omnibox-spacer.noprint"): break
                        except: pass
                        time.sleep(1)
                    else: 
                        anomaly = 'anomaly'
                        # self.fail("time out")
                        
                    try: 
                        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.widget-pane-section-result-details-container"))
                        driver.find_element_by_css_selector("div.widget-pane-section-result").click()
                    except AssertionError as e: 
                        self.verificationErrors.append([str(index),'result-details-container not found'])
                        not_found = True
    
                # GETING street_name
                if not not_found:
                    for i in range(1):
                        try:
                            if self.is_element_present(By.CSS_SELECTOR, "div.widget-pane-section-omnibox-spacer.noprint"): break
                        except: pass
                        time.sleep(1)
                        
                    try: 
                        self.assertFalse(self.is_element_present(By.CSS_SELECTOR, "div.widget-pane-section-disambiguation-right-cell"))
                    except AssertionError as e:
                        driver.find_element_by_css_selector("div.widget-pane-section-disambiguation-right-cell").click()
                        self.verificationErrors.append(str(index)+'. disambiguation street')
                        
                    for i in range(5):
                        try:
                            if self.is_element_present(By.CSS_SELECTOR, "div.widget-pane-section-header-description"): break
                        except: pass
                        time.sleep(1)
                    else:
                        anomaly = 'anomaly'                     
                        # self.fail("time out")
                    try: 
                        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.widget-pane-section-header-title"))
                        
                        try: 
                            self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.widget-pane-section-info-line"))
                            self.assertEqual("Tambahkan", driver.find_element_by_css_selector("div.widget-pane-section-info-line").text)
                            result = suggest_name
                        except AssertionError as e: 
                            try: 
                                self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "span.widget-pane-section-info-text > span > span"))
                                street_name = driver.find_element_by_css_selector("span.widget-pane-section-info-text > span > span").text
                                result = street_name
                            except AssertionError as e: self.verificationErrors.append([str(index),'not address'])
                    except AssertionError as e: 
                        anomaly = 'anomaly'
                        self.verificationErrors.append([str(index),'not entering location details'])
                    # place_name = driver.find_element_by_css_selector("div.widget-pane-section-header-title").text
                else:
                    anomaly = 'not found' 
                    print 'not found'
            except ElementNotVisibleException as e:
                anomaly = 'element is hidden'
                self.begin = index
                self.test()
            except (NoSuchElementException,StaleElementReferenceException) as e:
                anomaly = 'not load properly'
                self.begin = index
                self.test()
            except WebDriverException as e:
                anomaly = 'global exception'
                self.begin = index
                self.test()
            finally:
                print 'GMaps: ' + result
                if anomaly is not '': print anomaly
                writer.writerow([place,result,anomaly])
                if result == '':
                    driver.save_screenshot('error/'+str(self.index)+'.png')        
                else:
                    driver.save_screenshot('screenshot/'+str(index)+'.png')
                # index = index+1
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.save_screenshot('error/'+str(self.index)+'.png')        
        self.driver.quit()
        for err in self.verificationErrors:
            self.writerErr.writerow(err)
            self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
